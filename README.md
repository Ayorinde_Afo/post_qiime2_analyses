# The Gut Microbiota of an Individual Varies With Intercontinental Four-Month Stay Between Italy and Nigeria: A Pilot Study

## Name
Gut Microbiome project

## Description
This theoretically should point to the scripts I used for statistics and visualization for the study on Intercontinental Residence and the Gut Microbiota. Find a link to the paper [here](https://www.frontiersin.org/articles/10.3389/fcimb.2021.725769/full).


## Authors and acknowledgment
Myself and all co-authors.

## Project status
Project done.
